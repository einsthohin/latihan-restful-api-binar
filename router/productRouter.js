const express = require('express')
const router = express.Router()

// CRUD => Basic function
// Create
// Read
// Update
// Delete

let dataProduct = [
    {
        id : 1,
        namaProduct : "Macbook Pro 2015",
        namaBrand : "Apple",
        jumlahKetersediaan : 10
    },
    {
        id : 2,
        namaProduct : "Macbook Air 2015",
        namaBrand : "Apple",
        jumlahKetersediaan : 10
    },
    {
        id : 3,
        namaProduct : "iPhone 12 Pro Max",
        namaBrand : "Apple",
        jumlahKetersediaan : 10
    },
    {
        id : 4,
        namaProduct : "iPad Pro 2020",
        namaBrand : "Apple",
        jumlahKetersediaan : 10
    }
]

// API yang dimana bisa deliver sebuah requestt untuk membuat / create sebuah produk
router.post('/api/product', (req, res) => {
    const {namaProduct, namaBrand, jumlahKetersediaan} = req.body

    if(namaProduct === undefined || namaBrand === undefined || jumlahKetersediaan === undefined){
        res.status(400).json("Bad request because some data is not sent.")
        return
    }
    const nextId = dataProduct.length + 1

    dataProduct.push ({
        id : nextId,
        nama : namaProduct,
        brand : namaBrand,
        ketersediaan : jumlahKetersediaan
    })

    res.status(200).json("Sukses menambahkan product!")
    
});

// API yang dimana bisa deliver sebuah request untuk melihat semua data produk
router.get('/api/product', (req, res) => {
    res.status(200).json(dataProduct)
});

router.get('/api/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const filteredProduct = dataProduct.find(x => x.id == idProduct)

    //Validasi
    if(filteredProduct === undefined){
        res.status(404).json("Data product is not found!")
        return
    }
    res.status(200).json(filteredProduct)
})

// API yang dimana bisa deliver sebuah request untuk mengubah semua data produk
router.put('/api/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const {namaProduct, namaBrand, jumlahKetersediaan} = req.body
    
    if(namaProduct === undefined || namaBrand === undefined || jumlahKetersediaan === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }

    let isFound = false
    for(var x = 0; x<dataProduct.length; x++){
        if(dataProduct[x].id == idProduct){
            dataProduct[x].nama = namaProduct
            dataProduct[x].brand = namaBrand
            dataProduct[x].ketersediaan = jumlahKetersediaan

            isFound = true
            break
        }
    }

    if(isFound == false){
        res.status(404).json("Data product is not found!")
        return
    }else{
        res.status(200).json("Updated!")
        return
    }
});

// API yang dimana bisa deliver sebuah request untuk mendelete semua data / beberapa data produk
router.delete('/api/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct

    const searchProduct = dataProduct.find(x => x.id == idProduct)

    if (searchProduct === undefined){
        res.status(404).json("Data product is not found!")
        return
    }
    
    const index = dataProduct.indexOf(searchProduct)

    dataProduct.splice(index,1)

    res.status(200).json("Deleted!")
});

module.exports = router
